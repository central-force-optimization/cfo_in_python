## You must have matplotlib installed for this to work

## global variables
Accel	  = None
Best 	  = None
dim  	  = None
Mass 	  = None
userPath  = None
Position  = None
xMin	  = None
xMax	  = None
t         = None

KEY				='''_______KEY________
	"T" = Time
	"#" = Probe Index
	"P" = Probe Position
	"A" = Probe Acceleration
	"M" = Probe Mass\n'''

###################################################################################################
def outputSetup(_Best, _Position, _dim, _xMin, _xMax, _t, _path):
	global Best, dim, userPath, Position, xMin, xMax, t

	Best	 = _Best
	dim      = _dim
	userPath = _path
	Position = _Position
	xMin	 = _xMin
	xMax	 = _xMax
	t        = _t

	if not os.path.exists(userPath):	os.makedirs(userPath)

###################################################################################################

import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
import os

def plotImages(dim):
	if dim == 2:
		folder = userPath + 'Plots/'
		if not os.path.exists(folder):	os.makedirs(folder)

		x = [p[0] for p in Position]
		y = [p[1] for p in Position]

		pyplot.subplot(111)
		pyplot.scatter(x, y, c='b', marker='.')
		## add solution
		pyplot.scatter(Position[Best][0], Position[Best][1], c='r', marker=(5,2))
		# pyplot.show()
		pyplot.savefig('{}time_{}.png'.format(folder, t), bbox_inches='tight')
	elif dim == 3:
		fig = pyplot.figure()
		ax = fig.add_subplot(111, projection='3d')
		x = [p[0] for p in Position]
		y = [p[1] for p in Position]
		z = [p[1] for p in Position]
		ax.scatter(x, y, z, c='b', marker='.')
		pyplot.scatter(Position[Best][0], Position[Best][1], Position[Best][2], c='r', marker=(5,2))
		pyplot.show()

###################################################################################################

def main():
	dim = len(Position[len(Position) - 1])
	if dim == 2 or dim == 3:	plotImages(dim)