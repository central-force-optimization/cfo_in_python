import os

## Global Variables
Accel       = None
Best        = None
Factor      = None
Mass        = None
Position    = None
userPath    = None
xMin        = None
xMax        = None
function    = None
dim         = None
numProbes   = None
probeDist   = None
ALPHA       = None
BETA        = None
GAMMA       = None
density     = None
max_time    = None
accuracy    = None
curr        = None
prev        = None
file        = None

###################################################################################################

def CFO_A2(func, dimensions=2, probeDensity=3, distribution=1, maxTime=1000, min_x=-10, max_x=10, alpha=1, beta=3, gamma=0.5, solutionAccuracy=1E-3, output=None):
    global function, dim, density, ALPHA, BETA, GAMMA, maxAccel, max_time, accuracy, Accel, Position, Best, Mass, xMax, xMin, userPath, probeDist

    ## input validation
    if not isinstance(dimensions,        int):  exit("'dimensions' ({}) must be an integer".format(dim))
    if not isinstance(probeDensity, int):   exit("'probeDensity' ({})must be an integer".format(probeDensity))
    if not isinstance(distribution, int):   exit("'distribution' ({})must be an integer".format(distribution))
    if not isinstance(maxTime,           int):  exit("'time' ({}) must be an integer",format(time))
    if not isinstance(alpha, (int, float)): exit("'alpha' ({}) must be integer or float".format(alpha))
    if not isinstance(beta,  (int, float)): exit("'beta' ({})must be integer or float".format(beta))
    if not isinstance(gamma, (int, float)): exit("'gamma' ({})must be integer or float".format(gamma))

    function  = func
    dim       = dimensions
    density   = probeDensity
    probeDist = distribution
    ALPHA     = alpha
    BETA      = beta
    GAMMA     = gamma
    max_time  = maxTime
    accuracy  = solutionAccuracy

    if distribution < 0 or distribution > 2:        distribution = 2

    if isinstance(min_x, (int, float)): min_x = [min_x] * dim
    if isinstance(max_x, (int, float)): max_x = [max_x] * dim
    if len(min_x or max_x) != dim: exit("xMax/xMin must be an integer or a list with size equal to amount of dimensions")
    xMin = min_x
    xMax = max_x
    userPath = output

    createStructures(probeDist)
    run()

###################################################################################################

def createStructures(probeDist):
    global Accel, Best, Factor, Mass, Position, numProbes, curr, prev
    Best = [None, None]
    curr = 0
    prev = 1

    probeAmt = { 0:dim*density, 1:density, 2:density**dim }
    numProbes = probeAmt[probeDist]

    ## ArrayName[t mod 2][probe][dimension]
    Accel       = [ [ [ 0 for d in range(dim) ] for p in range(numProbes) ], []]
    Factor      = [ [ 1 for p in range(numProbes) ], [] ]
    Mass        = [ [ 0 for p in range(numProbes) ], [] ]
    Position    = [ [ [ 0 for d in range(dim) ] for p in range(numProbes) ], []]

    distPattern = { 0:uniformOnAxis, 1:uniformOnDiagonal, 2:hypercube }
    distPattern[probeDist]()
    updateMass()
    updateAccel()

###################################################################################################

def uniformOnAxis():
    global Position
    for p in range(numProbes):
        d, shift = divmod(p, density)
        spread = (xMax[d] - xMin[d]) / (density - 1)
        Position[0][p][d] = xMin[d] + shift * spread

def uniformOnDiagonal():
    global Position
    for d in range(dim):
        spread = (xMax[d] - xMin[d]) / (numProbes - 1)
        for p in range(numProbes):
            Position[0][p][d] = xMin[d] + p * spread

def hypercube():
    global Position, xMin, xMax
    for p in range(numProbes):
        remainder = p
        for d in range(dim):
            spacing = (xMax[d] - xMin[d]) / (density - 1)
            remainder, n = divmod(remainder, density)
            Position[0][p][d] = xMin[d] + (n * spacing)

###################################################################################################

def updateProbePosition():
  ## new = old + vt + (1/2)at^2, where v = 0 and t = 1
    Position[curr] = []
    Factor[curr]   = []
    for p in range(len(Position[prev])):
        if Factor[prev][p] == 0:    continue
        oldPos = Position[prev][p]
        newPos = [(a+b) for (a,b) in zip(oldPos, Accel[prev][p])]
        for d in range(dim):
            if newPos[d] > xMax[d]:     newPos[d] = oldPos[d] + 0.5*(xMax[d] - oldPos[d])
            if newPos[d] < xMin[d]:     newPos[d] = oldPos[d] + 0.5*(xMin[d] - oldPos[d])
        Position[curr].append(newPos)
        Factor[curr].append(Factor[prev][p])

###################################################################################################

def updateMass():
    Mass[curr] = []
    Best[curr] = 0
    tempBest= -float('inf')
    for p in range(len(Position[curr])):
        if Factor[curr][p] == 0: continue
        
        tempMass = function(Position[curr][p])
        if tempMass > tempBest:
            tempBest = tempMass
            Best[curr] = p
        Mass[curr].append(tempMass)

###################################################################################################

def hasConverged():
    ## Converged if 20% of probes are within 'accuracy' neighborhood
    inNbrhd = Factor[curr][Best[curr]]
    for p in range(len(Position[curr])):
        if p != Best[curr]:
            distance = 0

            for d in range(dim):
                distance += (Position[curr][p][d] - Position[curr][Best[curr]][d])**2
            if distance**0.5 < accuracy:
                inNbrhd += Factor[curr][p]
    if inNbrhd >= (0.2 * numProbes):    return True
    else:                                                           return False

###################################################################################################

def updateAccel():
    Accel[curr] = []
    for p in range(len(Position[curr])):
        if Factor[curr][p] == 0: continue
        tempAccel = [ 0 for d in range(dim) ]
        for d in range(dim):
            maxAccel = 0.1 * (xMax[d] - xMin[d])
            for k in range(len(Position[curr])):
                if k != p:
                    deltaMass = Mass[curr][k] - Mass[curr][p]
                    if deltaMass == 0: deltaMass = 1 ## probes of equal mass influence each other, this fixes / by 0 condition
                    if deltaMass > 0:
                        sumofsquares = 0
                        for d2 in range(dim):   sumofsquares += (Position[curr][p][d2] - Position[curr][k][d2])**2
                        if sumofsquares != 0:
                            deltaPosition = Position[curr][k][d] - Position[curr][p][d]
                            tempAccel[d] += GAMMA * deltaPosition * deltaMass**ALPHA /  sumofsquares**(0.5*BETA)
            if tempAccel[d] >    maxAccel: tempAccel[d] =    maxAccel
            if tempAccel[d] < -maxAccel: tempAccel[d] = -maxAccel
        Accel[curr].append(tempAccel)

###################################################################################################

def dump(step):
    text = 'time: {}\n'.format(step)
    file.write(text)
    for p in range(len(Position[curr])):
        text = '{:02d}\t{}\t{}\t{}\t{}\n'.format(p, Position[curr][p], Accel[curr][p], Mass[curr][p], Factor[curr][p])
        file.write(text)
    file.write('\n')

import threading, CFO_OutputHelper_A2
def run():
    global curr, prev, file

    benchfile = open(userPath, 'a')

##  ID = '_{}'.format(probeDist)
##  filename = 'c:/users/username/downloads/debug' + ID + '.txt'
##  file = open(filename, 'w')
##  freq = max_time // 10
    T = 0
    for t in range(1, max_time):
        curr, prev, T= prev, curr, t
        #combineFactors()
        updateProbePosition()
        updateMass()
        if hasConverged():
            #print('converged @ step: ', t)
            break
        updateAccel()
##      if  t % freq == 0:
##          dump(str(t) + '\n')
##          print('{}\tSolution: {}\n\tValue: {}'.format(t, Position[curr][Best[curr]], Mass[curr][Best[curr]]))
##          CFO_OutputHelper_A2.outputSetup(Best[curr], Position[curr], dim, xMin, xMax, t, userPath)
##          thrd = threading.Thread(target=CFO_OutputHelper_A2.main)
##          thrd.start()
    ##dump('LAST')
    benchfile.write('{},"{}",{},'.format(T, Position[curr][Best[curr]], Mass[curr][Best[curr]]))
    #print('\tSolution: {}; Value: {};'.format(Position[curr][Best[curr]], Mass[curr][Best[curr]]))

###################################################################################################
if __name__ == "__main__":  pass