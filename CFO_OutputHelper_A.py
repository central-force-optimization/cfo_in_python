## You must have matplotlib installed for this to work

## global variables
Accel       = None
Best            = None
dim           = None
Mass            = None
userPath    = None
Position  = None
xMin            = None
xMax            = None

KEY             ='''_______KEY________
    "T" = Time
    "#" = Probe Index
    "P" = Probe Position
    "A" = Probe Acceleration
    "M" = Probe Mass\n'''

###################################################################################################
def outputSetup(_Accel, _Best, _Position, _Mass, _dim, _xMin, _xMax, _path):
    Accel    = _Accel
    Best     = _Best
    dim      = _dim
    Mass     = _Mass
    userPath = _path
    Position = _Position   
    xMin     = _xMin
    xMax     = _xMax

    if not os.path.exists(path):    os.makedirs(path)

###################################################################################################

class probe(object):
    def __init__(self, a, p, m):
        self.accel      = a
        self.position = p
        self.mass           = m

    def reprNoAccel(self):
        pos = '[' + ', '.join(['%015.10f' % x for x in self.position]) + ']'
        return str("P: {}\t\t\tM: {:<23}\n".format(pos, self.mass))

    def __repr__(self):
        pos = '[' + ', '.join(['%015.10f' % x for x in self.position]) + ']'
        acc = '[' + ', '.join(['%015.10f' % x for x in self.accel]) + ']'
        return str("P: {}\t\tA: {}\t\tM: {:<23}\n".format(pos, acc, self.mass))

###################################################################################################

def printSolution():
    file = open(userPath + 'CFO_Solution.txt', 'w')

    last_time = max(Best)    # last time step
    file.write('Solution:\t{}\n'.format(Position[last_time][Best[last_time]]))
    file.write('Value: \t{}\n\n'.format(Mass[last_time][Best[last_time]]))

    file.write(KEY)

    file.write('Best Solutions:\n')
    for t in range(last_time + 1):
        p = probe(0, Position[t][Best[t]], Mass[t][Best[t]])
        file.write('T: {:6}\t\t#: {:4}\t\t{}'.format(t, Best[t], p.reprNoAccel()))
        del(p)
    file.close()

###################################################################################################

def printprobesByTime():
    file = open(userPath + 'CFO_probesByTime.txt', 'w')
    file.write(KEY)

    for t in range(max(Best) + 1):
        file.write('\nTime: {:4}\n'.format(t))
        for p in range(len(Position[t])):
            prb = probe(Accel[t][p], Position[t][p], Mass[t][p])
            file.write('{}'.format(prb))
    file.close()

###################################################################################################

import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
import os

def plotImages(dim):
    last_time = max(Best)    # last time step

    if last_time > 10:  freq = last_time // 10
    else:                               freq = 1

    if dim == 2:
        folder = userPath + 'Plots/'
        if not os.path.exists(folder):  os.makedirs(folder)

        for t in range(last_time + 1):
            if (t % freq == 0) or (t == last_time):
                x = [p[0] for p in Position[t]]
                y = [p[1] for p in Position[t]]

                pyplot.subplot(111)
                pyplot.scatter(x, y, c='b', marker='.')
                ## add solution
                pyplot.scatter(Position[t][Best[t]][0], Position[t][Best[t]][1], c='r', marker=(5,2))
            # pyplot.show()
                pyplot.savefig('{}time_{}.png'.format(folder, t), bbox_inches='tight')
    elif dim == 3:
        userFreq = input("How many images (in evenly spaced intervals throught the time space) would you like to view?")
        try:                                freq = int(userFreq)
        except ValueError:  freq = 10

        for t in range(last_time + 1):
            if (t % freq == 0) or (t == last_time):
                fig = pyplot.figure()
                ax = fig.add_subplot(111, projection='3d')
                x = [p[0] for p in Position[t]]
                y = [p[1] for p in Position[t]]
                z = [p[1] for p in Position[t]]
                ax.scatter(x, y, z, c='b', marker='.')
                pyplot.scatter(Position[t][Best[t]][0], Position[t][Best[t]][1], Position[t][Best[t]][2], c='r', marker=(5,2))
                pyplot.show()

###################################################################################################

def main():
    dim = len(Accel[len(Accel) - 1][ len(Accel[len(Accel) - 1]) - 1 ] )
    printSolution()
    printprobesByTime()
    if dim == 2 or dim == 3:    plotImages(dim)