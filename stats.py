import numpy as np
import scipy.stats as stats
file = None
def runstats():
	func			= []
	runtimes1 = []
	runtimes2 = []
	try:
		runtimefile = open(sourceFile, 'r')
		for line in runtimefile:
			splitline = line.split(',')
			func.append(splitline[0])
			runtimes1.append(splitline[1])
			runtimes2.append(splitline[2])
		runtimefile.close()
	except IOError:	return 'failed to open'

	for i in range((len(func))//10):
		xi = 10*i
		xj = xi + 10
		r1Array = np.array(runtimes1[xi:xj], np.float)
		r2Array = np.array(runtimes2[xi:xj], np.float)
		r1shifted = [i - min(r1Array) for i in r1Array]
		r2shifted = [i - min(r2Array) for i in r2Array]
		r1_results = stats.kstest(r1shifted, 'norm')
		r2_results = stats.kstest(r2shifted, 'norm')
		ttestResults = stats.ttest_ind(r1Array, r2Array, equal_var=False)
		a_output = ',{}_{}_Std,{},'.format(xi, xj, r1_results)
		o_output = '{}_{}_Mult,{},'.format(xi, xj, r2_results)
		t_output = '{}, {}\n'.format(ttestResults[0], ttestResults[1])
		file.write(func[xi])
		file.write(a_output.replace('(','').replace(')', ''))
		file.write(o_output.replace('(','').replace(')', ''))
		file.write(t_output)
	blank = '" ",'*8 + '" "\n'
	file.write(blank)

if __name__ == '__main__':
	file = open('C:/Users/UserName/Dropbox/CFO_Output/stats.txt', 'w')
	sourceFile = 'C:/users/username/dropbox/cfo_output/runtimecsv.txt'
	runstats()
	file.close()
	print('done')