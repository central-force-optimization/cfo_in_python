## Global Variables
userPath 	= None
function 	= None
probes   	= None
dim      	= None
probeDist	= None
xMin     	= None
xMin_orig	= None
xMax     	= None
xMax_orig	= None
numProbes	= None
ALPHA    	= None
BETA     	= None
GAMMA    	= None
density  	= None
max_time 	= None
accuracy 	= None
best     	= dict() ## best = {time: probeIndex}

###################################################################################################
def CFO(func, dimensions=2, probeDensity=3, distribution=2, maxTime=1000, min_x=-10, max_x=10, alpha=1, beta=3, gamma=0.5, solutionAccuracy=1E-3, output=None):
	global function, numProbes, dim, density, probeDist, ALPHA, BETA, GAMMA, xMin, xMax, xMin_orig, xMax_orig, maxAccel, max_time, userPath, accuracy

	## input validation
	if not isinstance(dimensions,		 int):	exit("'dimensions' ({}) must be an integer".format(dim))
	if not isinstance(probeDensity,	int):	exit("'probeDensity' ({})must be an integer".format(probeDensity))
	if not isinstance(distribution,	int):	exit("'distribution' ({})must be an integer".format(distribution))
	if not isinstance(maxTime,			 int):	exit("'time' ({}) must be an integer",format(time))
	if not isinstance(alpha, (int, float)): exit("'alpha' ({}) must be integer or float".format(alpha))
	if not isinstance(beta,	 (int, float)): exit("'beta' ({})must be integer or float".format(beta))
	if not isinstance(gamma, (int, float)): exit("'gamma' ({})must be integer or float".format(gamma))

	function 	= func
	dim      	= dimensions
	density  	= probeDensity
	probeDist	= distribution
	ALPHA    	= alpha
	BETA     	= beta
	GAMMA    	= gamma
	max_time 	= maxTime
	accuracy 	= solutionAccuracy

	if distribution < 0 or distribution > 2: distribution = 2

	if isinstance(min_x, (int, float)): min_x = [min_x] * dim
	if isinstance(max_x, (int, float)): max_x = [max_x] * dim
	if len(min_x or max_x) != dim: exit("xMax/xMin must be a list with 'dim' elements or an integer")
	xMin = min_x
	xMax = max_x

	userPath = output

	run()

###################################################################################################

class probe(object):
	def __init__(self, _id):
		self.id 			= _id
		self.position = [0.0] * dim
		self.accel		= [0.0] * dim
		self.mass			= 0.0E-7
		self.factor		= 1 ## multiplicity factor

	def __repr__(self):
		pos = '[' + ', '.join(['%015.10f' % x for x in self.position]) + ']'
		acc = '[' + ', '.join(['%015.10f' % x for x in self.accel]) + ']'
		return str("# {:<3} P: {}\t\tA: {}\t\tM: {:<23}\t x{}\n".format(self.id, pos, acc, self.mass, self.factor))

###################################################################################################

def initProbes():
	global probes, numProbes

	probeAmt = { 0:dim*density, 1:density, 2:density**dim }
	numProbes = probeAmt[probeDist]

	## probes[time][probe#]
	probes = [[probe(p) for p in range(numProbes)] for time in range(max_time)]

	distPattern = { 0:uniformOnAxis, 1:uniformOnDiagonal, 2:hypercube }
	distPattern[probeDist]()
	updateProbeMasses(0)
	updateAccel(0)

def uniformOnAxis():
	global probes
	for p in range(numProbes):
		d, shift = divmod(p, density)
		spread = (xMax[d] - xMin[d]) / (density - 1)
		probes[0][p].position[d] = xMin[d] + shift * spread
		# NEEDS TO REMOVE OVERLAPPING PROBES (only possible at the origin)

def uniformOnDiagonal():
	global probes
	for d in range(dim):
		spread = (xMax[d] - xMin[d]) / (numProbes - 1)
		for p in range(numProbes):
			probes[0][p].position[d] = xMin[d] + p * spread

def hypercube():
	global probes
	for p in range(numProbes):
		remainder = p
		for d in range(dim):
			spacing = (xMax[d] - xMin[d]) / (density - 1)
			remainder, n = divmod(remainder, density)
			probes[0][p].position[d] = xMin[d] + (n * spacing)

###################################################################################################

def shrinkSpace(t):
	## space is cut in 1/2 in all directions toward the best probe
	for d in range(dim):
		bestPos = probes[t][ best[t] ].position
		xMin[d] = xMin[d] + 0.5 * (bestPos[d] - xMin[d])
		xMax[d] = xMax[d] - 0.5 * (xMax[d] - bestPos[d])

	## reposition outliers within space
	for p in range(len(probes[t])):
		for d in range(dim):
			oldPos = probes[t - 1][p].position[d]
			if probes[t][p].position[d] > xMax[d]:	probes[t][p].position[d] = oldPos + 0.5*(xMax[d] - oldPos)
			if probes[t][p].position[d] < xMin[d]:	probes[t][p].position[d] = oldPos + 0.5*(xMin[d] - oldPos)

###################################################################################################

def updateProbePosition(t):
	global probes
	for p in range(len(probes[t])):
		for d in range(dim):
			oldPos = probes[t-1][p].position[d]
			## new = old + vt + (1/2)at^2, where v = 0 and t = 1
			newPos = oldPos + probes[t-1][p].accel[d]
			if newPos > xMax[d]:	newPos = oldPos + 0.5*(xMax[d] - oldPos )
			if newPos < xMin[d]:	newPos = oldPos - 0.5*(oldPos	- xMin[d])
			probes[t][p].position[d] = newPos

###################################################################################################

##def mergeNbrhds(t):
##	global probes
##	for p in range(len(probes[t])):
##		for q in range(p, len(probes[t])):
##			sumofsquares = 0
##			for d in range(dim):	sumofsquares += (probes[t][p].position[d] - probes[t][q].position[d])**2
##			if(sumofsquares**0.5 < accuracy):
##				combineProbes(probes[t][p].id, probes[t][q].id, t)
##				break
##
###################################################
##
##def combineProbes(pid, qid, t):
##	global probes
##	for t1 in range(t, max_time):
##		for p in range(len(probes[t1])):
##			if probes[t1][p].id == pid: probes[t1][p].factor +=


###################################################################################################

def hasConverged(t):
## Converged if 25% of total probes are within 1E-3 of each other
	inNbrhd = 0
	pivot = probes[t][best[t]].position
	for p in range(len(probes[t])):
		if p != best[t]:
			distance = 0
			for d in range(dim):
				distance += (probes[t][p].position[d] - pivot[d])**2
			distance = distance**0.5
			if distance < accuracy: inNbrhd += probes[t][p].factor

	if inNbrhd > (0.25 * numProbes): 	return True
	else:															return False

###################################################################################################

def updateProbeMasses(t):
	global probes, best

	## best = {time: probeIndex}
	best[t] = -1

	tempBest	= -float('inf')

	for p in range(len(probes[t])):
		probe = probes[t][p]
		x = probe.position
		m = 0
		m += function(x)
		probe.mass = m

		if	m > tempBest:
			tempBest	= m
			best[t]	 = p

###################################################################################################

def updateAccel(t):
	global probes

	for p in range(len(probes[t])):
		for d in range(dim):
			maxAccel = 0.1 * (xMax[d] - xMin[d])
			probes[t][p].accel[d] = 0
			for k in range(len(probes[t])):
				if k != p:
					deltaMass = probes[t][k].mass - probes[t][p].mass
					if deltaMass == 0: deltaMass = 1 ## probes of equal mass influence each other, this fixes / by 0 condition
					if deltaMass > 0:
						sumofsquares = 0
						for d2 in range(dim):	sumofsquares += (probes[t][p].position[d2] - probes[t][k].position[d2])**2
						if sumofsquares != 0:
							deltaPosition = probes[t][k].position[d] - probes[t][p].position[d]
							probes[t][p].accel[d] += (GAMMA * deltaPosition * probes[t][k].factor * deltaMass**ALPHA) /	sumofsquares**(0.5*BETA)
			if probes[t][p].accel[d] >	maxAccel: probes[t][p].accel[d] =	maxAccel
			if probes[t][p].accel[d] <- maxAccel: probes[t][p].accel[d] = -maxAccel

###################################################################################################
import threading, CFO_OutputHelper_O

def run():
	global probes, numProbes

	initProbes()

	#shrinkInterval = max_time // 10
	for t in range(1, max_time):
		updateProbePosition(t)
		updateProbeMasses(t)
		#mergeNbrhds(t) # REPAIR
		if hasConverged(t): 
			print('converged')
			break
		updateAccel(t)
		#if t % shrinkInterval == 0: shrinkSpace(t)

	T = max(best) # last time step
	print('\tSolution: {}'.format(probes[T][ best[T] ].position))
	print('\tValue: {}'.format(		probes[T][ best[T] ].mass		 ))

	if userPath:
		try:
			CFO_OutputHelper_O.outputSetup(probes, dim, xMin, xMax, best, userPath)
			thrd = threading.Thread(target=CFO_OutputHelper_O.main)
			thrd.start()
		except:	 print('CFO_OutputHelper_O.py not located')

###################################################################################################

if __name__ == "__main__": pass