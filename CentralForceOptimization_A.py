import os

## Global Variables
Accel       = None
Best        = dict()
Mass        = None
Position    = None
userPath    = None
xMin        = None
xMax        = None
function    = None
dim         = None
numProbes   = None
ALPHA       = None
BETA        = None
GAMMA       = None
density     = None
max_time    = None
accuracy    = None

###################################################################################################
def CFO_A(func, dimensions=2, probeDensity=3, distribution=2, maxTime=1000, min_x=-10, max_x=10, alpha=1, beta=3, gamma=0.5, solutionAccuracy=1E-3, output=None):
    global function, dim, density, ALPHA, BETA, GAMMA, maxAccel, max_time, accuracy, Accel, Position, Best, Mass, xMax, xMin

    ## input validation
    if not isinstance(dimensions,   int):   exit("'dimensions' ({}) must be an integer".format(dim))
    if not isinstance(probeDensity, int):   exit("'probeDensity' ({})must be an integer".format(probeDensity))
    if not isinstance(distribution, int):   exit("'distribution' ({})must be an integer".format(distribution))
    if not isinstance(maxTime,      int):   exit("'time' ({}) must be an integer",format(time))
    if not isinstance(alpha, (int, float)): exit("'alpha' ({}) must be integer or float".format(alpha))
    if not isinstance(beta,  (int, float)): exit("'beta' ({})must be integer or float".format(beta))
    if not isinstance(gamma, (int, float)): exit("'gamma' ({})must be integer or float".format(gamma))

    function    = func
    dim         = dimensions
    density     = probeDensity
    probeDist   = distribution
    ALPHA       = alpha
    BETA        = beta
    GAMMA       = gamma
    max_time    = maxTime
    accuracy    = solutionAccuracy

    if distribution < 0 or distribution > 2:        distribution = 2

    if isinstance(min_x, (int, float)): min_x = [min_x] * dim
    if isinstance(max_x, (int, float)): max_x = [max_x] * dim
    if len(min_x or max_x) != dim: exit("xMax/xMin must be an integer or a list with size equal to amount of dimensions")
    xMin     = min_x
    xMax     = max_x
    userPath = output

    createStructures(probeDist)
    run()

###################################################################################################

def createStructures(probeDist):
    global Accel, Mass, Position, numProbes

    probeAmt = { 0:dim*density, 1:density, 2:density**dim }
    numProbes = probeAmt[probeDist]

    ## ArrayName[time][probe][dimension]
    Accel    = [ [ [ 0 for d in range(dim) ] for p in range(numProbes) ] for t in range(max_time)]
    Mass     = [ [ 0 for p in range(numProbes) ] for t in range(max_time) ]
    Position = [ [ [ 0 for d in range(dim) ] for p in range(numProbes) ] for t in range(max_time)]

    distPattern = { 0:uniformOnAxis, 1:uniformOnDiagonal, 2:hypercube }
    distPattern[probeDist]()
    updateProbeMasses(0)
    updateAccel(0)

## max_time     = len(Accel)
## numProbes    = len(Accel[len(Accel) - 1])
## dimensions = len(Accel[len(Accel) - 1][ len(Accel[len(Accel) - 1]) - 1 ] )

###################################################################################################

def uniformOnAxis():
    global Position
    for p in range(numProbes):
        d, shift = divmod(p, density)
        spread = (xMax[d] - xMin[d]) / (density - 1)
        Position[0][p][d] = xMin[d] + shift * spread

def uniformOnDiagonal():
    global Position
    for d in range(dim):
        spread = (xMax[d] - xMin[d]) / (numProbes - 1)
        for p in range(numProbes):
            Position[0][p][d] = xMin[d] + p * spread

def hypercube():
    global Position, xMin, xMax
    for p in range(numProbes):
        remainder = p
        for d in range(dim):
            spacing = (xMax[d] - xMin[d]) / (density - 1)
            remainder, n = divmod(remainder, density)
            Position[0][p][d] = xMin[d] + (n * spacing)

###################################################################################################s

def updateProbePosition(t):
    for p in range(len(Position[t])):
        for d in range(dim):
            oldPos = Position[t-1][p][d]
            ## new = old + vt + (1/2)at^2, where v = 0 and t = 1
            newPos = oldPos + Accel[t-1][p][d]
            if newPos > xMax[d]:    newPos = oldPos + 0.5*(xMax[d] - oldPos)
            if newPos < xMin[d]:    newPos = oldPos + 0.5*(xMin[d] - oldPos)
            Position[t][p][d] = newPos

###################################################################################################

def hasConverged(t):
## Converged if 25% of total probes are within 1E-3 of each other
    inNbrhd = 0
    for p in range(len(Position[t])):
        if p != Best[t]:
            distance = 0
            for d in range(dim):
                distance += (Position[t][p][d] - Position[t][Best[t]][d])**2
            distance = distance**0.5
            if distance < accuracy: inNbrhd += 1

    if inNbrhd > (0.25 * numProbes):    return True
    else:                                                           return False

###################################################################################################

def updateProbeMasses(t):
    ## Best = {time: probeIndex}
    Best[t] = -1
    tempBest= -float('inf')

    for p in range(len(Mass[t])):
        Mass[t][p] += function(Position[t][p])

        if Mass[t][p] > tempBest:
            tempBest = Mass[t][p]
            Best[t] = p

###################################################################################################

def testAccel(t):
    for p in range(len(Accel[t])):
        for d in range(dim):
            maxAccel = 0.1 * (xMax[d] - xMin[d])
            Accel[t][p][d] = 0
            for k in range(len(Accel[t])):
                if k != p:
                    deltaMass = Mass[t][k] - Mass[t][p]
                    if deltaMass == 0: deltaMass = 1 ## probes of equal mass influence each other, this fixes / by 0 condition
                    if deltaMass > 0:
                        deltaPosition = Position[t][k][d] - Position[t][p][d]
                        if deltaPosition > 0:
                            Accel[t][p][d] += GAMMA * deltaMass**ALPHA / deltaPosition**BETA
            if Accel[t][p][d] > maxAccel:  Accel[t][p][d] =  maxAccel
            if Accel[t][p][d] < -maxAccel: Accel[t][p][d] = -maxAccel


def updateAccel(t):
    for p in range(len(Accel[t])):
        for d in range(dim):
            maxAccel = 0.1 * (xMax[d] - xMin[d])
            Accel[t][p][d] = 0
            for k in range(len(Accel[t])):
                if k != p:
                    deltaMass = Mass[t][k] - Mass[t][p]
                    if deltaMass == 0: deltaMass = 1 ## probes of equal mass influence each other, this fixes / by 0 condition
                    if deltaMass > 0:
                        sumofsquares = 0
                        for d2 in range(dim):   sumofsquares += (Position[t][p][d2] - Position[t][k][d2])**2
                        if sumofsquares != 0:
                            deltaPosition = Position[t][k][d] - Position[t][p][d]
                            Accel[t][p][d] += (GAMMA * deltaPosition * deltaMass**ALPHA) /  sumofsquares**(0.5*BETA)
            if Accel[t][p][d] > maxAccel: Accel[t][p][d] =   maxAccel
            if Accel[t][p][d] < -maxAccel: Accel[t][p][d] = -maxAccel

###################################################################################################
import threading, CFO_OutputHelper_A

def run():
    for t in range(1, max_time):
        updateProbePosition(t)
        updateProbeMasses(t)
        if hasConverged(t):
            print('converged @ step: ', t)
            break
        #testAccel(t)
        updateAccel(t)
        #if t % shrinkInterval == 0: shrinkSpace(t)

    T = max(Best) # last time step
    print('\tSolution: {}\n\tValue: {}'.format(Position[T][Best[T]], Mass[T][Best[T]]))

    if userPath:
        try:
            CFO_OutputHelper_A.outputSetup(Accel, Best, Position, Mass, dim, xMin, xMax, userPath)
            thrd = threading.Thread(target=CFO_OutputHelper_A.main)
            thrd.start()
        except:  print('CFO_OutputHelper_A.py not located')

###################################################################################################

if __name__ == "__main__": pass