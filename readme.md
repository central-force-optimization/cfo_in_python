CFO_In_Python is a Python implementation of the Central Force Optimization (CFO) algorithm. It is a deterministic metahueristic search algorithm which finds the maximum solution to a given function.

## Basic Usage
At a minimum, the module __CentralForceOptimization.py must be imported__. It can be called by its constructor: 
> __def CFO(functionName, dimensions=2, probeDensity=3, distrubution=2, maxTime=1000, min_x=-10, max_x=10, alpha=1, beta=3, gamma=0.5, output=None):__

The variants of the CFO algorithm include CFO_A (Array Based CFO), CFO_A2 (Array with only 2 time steps), CFO_T (Multiplicity Factor), and CFO_O (Object Oriented)


>Example:
>> CFO(myFunc, 2, 3, 0, 1000, -32, 32, 2, -2, 0.75, 'C:/myFunc/')
>>>def myFunc(x):
	y = 0
	for d in range(len(x)):		y += x[d]**2
	return -y
	
__*functionName *__ denotes the name of the function which you wish to be evaluated. This must accept a list parameter. __This version of the CFO algorithm is designed to find a maximum solution__, if you need to find a minimum, you must modify your function accordingly.
>Example:
>> def functionName(x):
	y = 0
	for d in range(dim):		y += x[d]**2
	return -y
	
 >or
>> def functionName(x):  return -(x[0]**2 -3*x[1])

__*dimensions*__ should match the length of the list that your function is designed to accept.

__*probeDensity*__ is the amount of probes distributed throughout the search space, this is probes per axis, diagonal, or hypercube axis.

__*distribution*__ sets the initial probe distribution pattern. 
>	0 : uniform on axis

>	1 : uniform on diagonal 

>	2 : hypercube 

__*maxTime*__ places a time limit on how long the algorithm is allowed to work. Note: Some functions may not converge, it is a good idea to put some time limit on the algorithm, even if you expect it to converge [ie. 50,000].

__*min_x/max_x*__ is an ordered list or integer. If an integer is input, the axis minimums will be uniform. If a list is input, the axis minimums will follow the list. 
>Example:
>> for 2 dimensions, min_x = -1, and max_x = 3:
>>> -1 <= x[0] <= 3 and -1 <= x[1] <= 3

>> for 2 dimensions, min_x = -1, and max_x = [2, 3]: 
>>> -1 <= x[0] <= 2 and -1 <= x[1] <= 3

__*alpha/beta/gamma*__ will be the values used to update each probe's acceleration as follows: 
>(gamma x deltaPosition x deltaMass**alpha) / sumOfSquares**(0.5*beta)

__*output*__ is the folder where detailed output will be sent (*CFO_OutputHelper.py* must also be loaded). This parameter may be left blank if you do not wish to see each probe's trace and other detailed information.

## Other Modules

####CFO_OutputHelper.py
This module contains methods which can be used to print detailed solutions to a set of files at the location specified in the *output* parameter of the CFO constructor. If the output location is specified and this module is loaded, the following will be output:

- A list of best solutions
- A list of probe positions by time
- Image plots (2D only) for 10 (equally spaced) time periods

#### Benchmarking.py
This module contains the functions used to test the algorithm and may serve as a good source for sample usage.
