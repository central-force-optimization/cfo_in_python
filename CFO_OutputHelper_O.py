## You must install matplotlib for this to work
from CentralForceOptimization_O import probe
import os

## global variables
probes      = None
dim             = None
xMin            = None
xMax            = None
best            = None
path            = None
KEY             ='''_______KEY________
    "T" = Time
    "#" = Probe Index
    "P" = Probe Position
    "A" = Probe Acceleration
    "M" = Probe Mass\n'''

###################################################################################################

def outputSetup(_probes, _dim, _xMin, _xMax, _best, _path):
    global probes, dim, xMin, xMax, best, path

    probes = _probes
    dim    = _dim
    best   = _best
    path   = _path
    xMin   = _xMin
    xMax   = _xMax

    if not os.path.exists(path):    os.makedirs(path)

###################################################################################################

def printSolution():
    file = open(path + 'CFO_Solution.txt', 'w')

    last_time = max(best)    # last time step
    solution = probes[last_time][ best[last_time] ]
    file.write('Solution:\t{}\n'.format(solution.position))
    file.write('Value: \t{}\n\n'.format(solution.mass))

    file.write(KEY)

    file.write('best Solutions:\n')
    for t in range(last_time + 1):   file.write('T: {:6}\t\t#: {:4}\t\t{}'.format(t, best[t], probes[t][best[t]]))
    file.close()

###################################################################################################

def printprobesByTime():
    global probes, best

    file = open(path + 'CFO_probesByTime.txt', 'w')
    file.write(KEY)

    for t in range(max(best) + 1):
        file.write('\nTime: {:4}\n'.format(t))
        for p in probes[t]:
            file.write('\t{}'.format(p))
    file.close()

###################################################################################################

import matplotlib.pyplot as pyplot
#from mpl_toolkits.mplot3d import Axes3D
#def plotImages3D():
#   fig = pyplot.figure()
#   ax = fig.add_subplot(111, projection='3d')
#   x = [p[0] for p in Position[t]]
#   y = [p[1] for p in Position[t]]
#   z = [p[1] for p in Position[t]]
#   ax.scatter(x, y, z, c='b', marker='.')
#   ax.scatter(0, 0, c='r', marker=(5,2))
#   pyplot.show()
#
#
#def plotImages():
#   global probes, best, path
#
#   if dim != 2: return
#
#   ## create folder for plots
#   path = path + 'Plots/'
#   if not os.path.exists(path):    os.makedirs(path)
#
#   last_time = max(best)    # last time step
#
#   ##  color = probe index
#   color = [p for p in range(len(probes[0]))]
#   opacity = 0.75
#
#   if last_time > 10:      freq = last_time // 10
#   else:                                freq = 1
#
#   arrow = dict(edgecolor='black', facecolor='white')
#   arrowLength = [0.1*(xMax[0] - xMin[0]), 0.1*(xMax[1] - xMin[1])]
#   for t in range(last_time + 1):
#       if (t % freq == 0) or (t == last_time):
#           x = [p.position[0] for p in probes[t]]
#           y = [p.position[1] for p in probes[t]]
#           color = [p.id for p in probes[t]]
#           pyplot.figure()
#           pyplot.plot(x, y, 'r.')
#           pyplot.axis([xMin[0], xMax[0], xMin[1], xMax[1]])
#           solution = probes[t][best[t]].position
#           if t == last_time:
#               ## zoom in
#               arrowLength = [(s + 1) for s in solution]
#               pyplot.axis([solution[0] - 0.1, solution[0] + 0.1, solution[1] - 0.1, solution[1] + 0.1])
#           ## draw arrow to solution
#           pyplot.annotate('', xy=(solution), xytext=(arrowLength), arrowprops=arrow,)
#           pyplot.savefig('{}time_{}.png'.format(path, t), bbox_inches='tight')
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D

def plotImages(dim):
    last_time = max(best)    # last time step

    if last_time > 10:  freq = last_time // 10
    else:                               freq = 1

    if dim == 2:
        folder = path + 'Plots/'
        if not os.path.exists(folder):  os.makedirs(folder)

        for t in range(last_time + 1):
            if (t % freq == 0) or (t == last_time):
                x = [p.position[0] for p in probes[t]]
                y = [p.position[1] for p in probes[t]]

                pyplot.subplot(111)
                pyplot.scatter(x, y, c='b', marker='.')
                solution = probes[t][best[t]].position
                pyplot.scatter(solution[0], solution[1], c='r', marker=(5,2))
            # pyplot.show()
                pyplot.savefig('{}time_{}.png'.format(folder, t), bbox_inches='tight')
    elif dim == 3:
        userFreq = input("How many images (in evenly spaced intervals throught the time space) would you like to view?")
        try:                                freq = int(userFreq)
        except ValueError:  freq = 10

        for t in range(last_time + 1):
            if (t % freq == 0) or (t == last_time):
                fig = pyplot.figure()
                ax = fig.add_subplot(111, projection='3d')
                x = [p.position[0] for p in probes[t]]
                y = [p.position[1] for p in probes[t]]
                y = [p.position[2] for p in probes[t]]
                ax.scatter(x, y, z, c='b', marker='.')
                pyplot.scatter(solution[0], solution[1], solution[2], c='r', marker=(5,2))
                pyplot.show()



###################################################################################################

def main():
    printSolution()
    printprobesByTime()
    if dim == 2 or dim == 3:    plotImages(dim)