import os, timeit, TestFunctions
from CentralForceOptimization_O import CFO
from CentralForceOptimization_A import CFO_A
from CentralForceOptimization_A2 import CFO_A2
from CentralForceOptimization_T import CFO_T

###################################################################################################

def bench(v):
    version = {'CFO':CFO, 'CFO_A':CFO_A, 'CFO_A2':CFO_A2, 'CFO_T':CFO_T}
    version[v](func,    dim,    dense,  distr, maxTime, axisMin, axisMax,   alpha, beta,    gamma,  accur, filename)

###################################################################################################

def run(version):
    setupstr = 'from __main__ import bench; v="' + version + '"'
    t = timeit.Timer('bench(v)', setup=setupstr)
    rt = runtime = t.timeit(number=1)
    (m, runtime) = divmod(runtime, 60)
    (s, runtime) = divmod(runtime, 1)
    (ms,runtime) = divmod(runtime, 1E-3)
    (us,runtime) = divmod(runtime, 1E-6)
    (ns,runtime) = divmod(runtime, 1E-9)
    print('\t\t{}\tRuntime (m:s.ms us ns): {:3d}:{:02d}.{:03d} {:03d} {:03d}'.format(version, int(m), int(s), int(ms), int(us), int(ns)))

    if True:
        if not os.path.exists(filename):    os.makedirs(filename)
    ## runtimeA,runtimeO,accuracy,dim,density,distribution,xMin,xMax,alpha,beta,gamma,maxTime
        file = open(filename, 'a')
        timeInfo = str(rt) + ','
        file.write(timeInfo)
        if version == 'CFO_T':
            paramInfo = '\t\t{},{},{},{},{},{},{},{},{},{}\n'.format(accur, dim, dense, distr, axisMin, axisMax, alpha, beta, gamma, maxTime)
            file.write(paramInfo)
        file.close()

###################################################################################################

import sched, time
def autorun(runs = 10):
    s = sched.scheduler(time.time, time.sleep)
    for i in range(runs):
        print('\tRUN: ', i+1, '/', runs)
        s.enter(10, i, run('CFO_A2'), None)
        s.enter(10, i, run('CFO_T'), None)

if __name__ == '__main__':
  ## dense -> 0:dim*density,        1:density,                      2:density**dim
    ## distr -> 0:uniformOnAxis,    1:uniformOnDiagonal,    2:hypercube
    path        = 'C:/users/username/dropbox/CFO_Output/'
    T = TestFunctions
    accur       = 1E-3
    alpha       = 1
    beta        = 2
    gamma       = 0.5

    ########################## 2d ##########################
    auto_dis = [    0,  1,  2]
    auto_dim = [    2,  2,  2]
    auto_den = [    25, 50, 7]
    maxTime  = 5000
    ##########################
    # SET 2D.1 COMPLETE
    # requires: axisMax =  auto_max[f]
    #auto_func = [T.f04, T.f05]
    #auto_min  = [[-15, -3], [-3, -2]]
    #auto_max  = [[  5,  3], [ 3,  2]]

    # SET 2D.2
    auto_func = [T.f02, T.f03,  T.f06, T.f07, T.f09, T.f10, T.f13]
    auto_min    = [ -4.5,   -10,   -100,    -2,   -10,-10.24,  -512]

    ########################## 2d ##########################
  ##############################################################################
    ########################## nd ##########################
    #auto_dis = [   0,      0,  0,  1,  1,  1,  2,  2]
    #auto_dim = [   2,      5,  10, 3,  5,  10, 3,  4]
    #auto_den = [   25,  10,  5,    50, 50, 50, 4,  3]
    #maxTime  = 1000
    ##########################
    # SET nD.1 COMPLETE
    #auto_func = [T.f01, T.f08, T.f11, T.f12, T.f14, T.f15, T.f16]
    #auto_min  = [   -32,    -512,-10.24, -1.28, -5.12,   -64,-2.048]

    # SET nD.2 COMPLETE
    #auto_func = [T.f17, T.f18, T.f19, T.f20, T.f21, T.f22, T.f23]
    #auto_min  = [ -100,  -100,  -512,  -100,    -10, -5.12,-10.24]

    ########################## nd ##########################

    for f in range(len(auto_func)):
        func        =  auto_func[f]
        axisMin =  auto_min[f]
        axisMax = -auto_min[f]
        filename = path + str(func)[10:13] + '_MultVsStd_runtime.txt'
        for i in range(len(auto_dis)):
            print('Config: ', i+1, '/', len(auto_dis), '\t', 'Func ', f+1, '/', len(auto_func))
            distr = auto_dis[i]
            dim     = auto_dim[i]
            dense = auto_den[i]
            autorun(10)